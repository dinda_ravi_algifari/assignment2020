﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel;

namespace DataAccess
{
    public class PenjualanRepo
    {
        //Get All
        public static List<DetailPenjualanViewModel> Alll()
        {
            List<DetailPenjualanViewModel> result = new List<DetailPenjualanViewModel>();
            using (var db = new Context())
            {                
                result = (from c in db.Barangs
                          join d in db.DetailPenjualans on c.kode_barang equals d.kode_barang into temp
                          from t in temp.DefaultIfEmpty()
                          join e in db.Penjualans on t.no_nota equals e.no_nota into temp2
                          from t2 in temp2.DefaultIfEmpty()
                          select new DetailPenjualanViewModel                          
                          {
                              id = t.id,
                              no_nota = t.no_nota,
                              nama_konsumen = t2.nama_konsumen,
                              nama_barang = c.nama_barang,
                              kode_barang = c.kode_barang,
                              quantity = t.quantity,
                              stock = c.stock,
                              harga = c.harga,                              
                              subtotal = t.subtotal,

                              created_at = c.created_at,
                              created_by = c.created_by,
                              modified_at = c.modified_at,
                              modified_by = c.modified_by,
                              is_delete = c.is_delete,
                              deleted_at = c.deleted_at,
                              deleted_by = c.deleted_by
                          }).ToList();
            }
            return result;
        }


        //Get All
        public static List<BarangViewModel> All()
        {
            List<BarangViewModel> result = new List<BarangViewModel>();
            using (var db = new Context())
            {
                result = (from c in db.Barangs
                          select new BarangViewModel
                          {
                              barang_id = c.barang_id,
                              kode_barang = c.kode_barang,
                              nama_barang = c.nama_barang,
                              satuan = c.satuan,
                              stock = c.stock,
                              harga = c.harga,

                              created_at = c.created_at,
                              created_by = c.created_by,
                              modified_at = c.modified_at,
                              modified_by = c.modified_by,
                              is_delete = c.is_delete,
                              deleted_at = c.deleted_at,
                              deleted_by = c.deleted_by
                          }).ToList();
            }
            return result;
        }


    }
}
