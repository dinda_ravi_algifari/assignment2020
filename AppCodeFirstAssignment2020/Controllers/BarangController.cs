﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel;

namespace AppCodeFirstAssignment2020.Controllers
{
    public class BarangController : Controller
    {
        // GET: Barang
        public ActionResult Index()
        {
            return View();
        }

        //Get List Barang
        public ActionResult ListBarang()
        {
            return PartialView("_ListBarang",BarangRepo.All());
        }

        //Get Create Barang
        public ActionResult Create() 
        {
            BarangViewModel model = new BarangViewModel();
            //model.barang_id = DetailPenjualanRepo.NoNota();
            return PartialView("_Create", model);
        }

        [HttpPost]
        public ActionResult Create(BarangViewModel model)
        {
            ResponseResult result = BarangRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                massage = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }


        // GEt : Edit
        public ActionResult Edit(int id)
        {
            //id => Category Id
            return PartialView("_Edit", BarangRepo.ById(id));
        }
        [HttpPost]
        public ActionResult Edit(BarangViewModel model)
        {
            ResponseResult result = BarangRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }

    }
}