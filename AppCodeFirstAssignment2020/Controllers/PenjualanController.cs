﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AppCodeFirstAssignment2020.Controllers
{
    public class PenjualanController : Controller
    {
        // GET: Penjualan
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Customer()
        {
            return PartialView("_Customer");
        }

        // Get : List Barang untuk beli
        public ActionResult ListShop()
        {
            return PartialView("_ListShop", PenjualanRepo.All());
        }

    }
}