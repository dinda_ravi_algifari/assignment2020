﻿using DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel;

namespace AppCodeFirstAssignment2020.Controllers
{
    public class DetailPenjualanController : Controller
    {       
        // GET: DetailPenjualan
        public ActionResult Index()
        {
            return View();
        }
        
        //choose user admin
        public ActionResult Admin()
        {
            return PartialView();
        }

        //Get Menu Admin
        public ActionResult Menu()
        {
            return PartialView("_Menu");
        }

        //choose user customer
        public ActionResult Customer()
        {
            return PartialView();
        }

        // Get : list
        public ActionResult List()
        {
            return PartialView("_List", DetailPenjualanRepo.All());
        }


        // Get : Create
        public ActionResult Create()
        {
            DetailPenjualanViewModel model = new DetailPenjualanViewModel();
            model.no_nota = DetailPenjualanRepo.NoNota();
            return PartialView("_Create", model);
        }

        [HttpPost]
        public ActionResult Create(DetailPenjualanViewModel model)
        {
            ResponseResult result = DetailPenjualanRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                massage = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }

        // GEt : Edit
        public ActionResult Edit(int id)
        {
            //id => Category Id
            return PartialView("_Edit", DetailPenjualanRepo.ById(id));
        }
        [HttpPost]
        public ActionResult Edit(DetailPenjualanViewModel model)
        {
            ResponseResult result = DetailPenjualanRepo.Update(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }

        // Get : Delete
        public ActionResult Delete(int id)
        {
            return PartialView("_Delete", DetailPenjualanRepo.ById(id));
        }

        [HttpPost]
        public ActionResult Delete(DetailPenjualanViewModel model)
        {
            ResponseResult result = DetailPenjualanRepo.Delete(model);
            return Json(new
            {
                success = result.Success,
                message = result.Message,
                entity = result.Entity
            }, JsonRequestBehavior.AllowGet);
        }

        //Get Details
        public ActionResult Details(int id)
        {
            return PartialView("_Details", DetailPenjualanRepo.ById(id));
        }

        //Get Nota
        public ActionResult Nota(int id)
        {
            return PartialView("_Nota", DetailPenjualanRepo.DetailNota(id));
        }
    }
}